package kz.aitu.oop.examples.assignment7;

abstract public class Shape {
    protected String color;
    protected Boolean filled;

    public Shape(String color, Boolean filled){
        this.color=color;
        this.filled=filled;
    }
    public Shape(){
        color="red";
        filled=true;
    }
    public String getColor(){
        return color;
    }
    public void setColor(String color){
        this.color=color;
    }
    public boolean isFilled(){
        return filled;
    }
    public void setFilled(boolean filled){
        this.filled=filled;
    }
    abstract public double getArea();
    abstract public double getPerimeter();
}
