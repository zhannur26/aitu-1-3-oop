package kz.aitu.oop.examples.assignment7;

public class Circle extends Shape{
    public double radius;
    public Circle(){
        radius=1.0;
    }
    public Circle(double radius, String color, boolean filled){
        super(color,filled);
    }
    public double getRadius(){
        return radius;
    }
    public void setRadius(double radius){
        this.radius=radius;
    }

    public double getArea() {
        return 0;
    }

    public double getPerimeter() {
        return 0;
    }

    public String toString() {
        return "Circle[Shape[color=?, filled=?],width=?,length=?]";
    }
}
