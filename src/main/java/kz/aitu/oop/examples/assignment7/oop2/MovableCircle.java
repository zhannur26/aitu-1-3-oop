package kz.aitu.oop.examples.assignment7.oop2;

public class MovableCircle implements Movable {
    public int radius;
    public MovablePoint center;
    public MovableCircle(int x, int y, int xSpeed, int ySpeed,int radius, MovablePoint center){
        this.radius=radius;
        this.center=center;
    }

    @Override
    public void moveUp() {
        center.moveUp();
    }

    @Override
    public void moveDown() {
        center.moveDown();
    }

    @Override
    public void moveRight() {
        center.moveRight();
    }

    @Override
    public void moveLeft() {
        center.moveLeft();
    }

    @Override
    public String toString() {
        return "MovableCircle{" +
                "radius=" + radius +
                ", center=" + center +
                '}';
    }
}
