package kz.aitu.oop.examples.assignment7;

public class Rectangle extends Shape{
    public double width;
    public double length;
    public Rectangle(){
        width=1.0;
        length=1.0;
    }
    public Rectangle(double width, double length){

    }
    public Rectangle(double width, double length, String color, boolean filled){
        super (color, filled);
        this.width=width;
        this.length=length;
    }
    public double getWidth(){
        return width;
    }
    public void setWidth(double width){
        this.width=width;
    }
    public double getLength(){
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        return 0;
    }

    @Override
    public double getPerimeter() {
        return 0;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + getWidth() +
                ", length=" + getLength() +
                '}';
    }
}
