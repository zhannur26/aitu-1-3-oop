package kz.aitu.oop.examples.assignment7.oop2;

public interface Movable {
    abstract public void moveUp();
    abstract public void moveDown();
    abstract public void moveLeft();
    abstract public void moveRight();

}
